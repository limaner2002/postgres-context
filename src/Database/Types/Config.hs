{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Database.Types.Config
  ( Config
  , HasConfig (..)
  ) where

import Control.Lens (Lens')
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Generics.Labels ()
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.Time
import GHC.Generics

data Config = Config
  { connectionString :: ByteString
  , numStripes :: Int
  , poolIdleTime :: NominalDiffTime
  , maxConnections :: Int
  } deriving (Eq, Generic, Ord, Show)

class HasConfig env where
  getConfigL :: Lens' env Config

instance HasConfig Config where
  getConfigL = id

instance FromJSON Config where
  parseJSON = withObject "Database Config Object" $ \o ->
    Config <$> (encodeUtf8 <$> o .: "db_connection_url")
           <*> o .: "num_stripes"
           <*> o .: "pool_idle_time"
           <*> o .: "max_connections"

instance ToJSON Config where
  toJSON Config {..} = object
    [ "db_connection_url" .= decodeUtf8 connectionString
    , "num_stripes" .= numStripes
    , "pool_idle_time" .= poolIdleTime
    , "max_connections" .= maxConnections
    ]
