{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedLabels #-}

module Context.Database
  ( Context
  , DBConn (..)
  , HasContext (..)
  , mkContext
  , withPoolMap
  ) where

import Control.Lens hiding (Context)
import Control.Monad.IO.Class
import Control.Monad.Reader.Class
import Data.Pool (Pool, createPool, withResource)
import Database.PostgreSQL.Simple as Simple
import Database.Types
import qualified Hasql.Connection as HAS

data DBConn a where
  SimpleConn :: DBConn Simple.Connection
  HasqlConn :: DBConn HAS.Connection

class HasContext env where
  context :: Lens' env Context

instance HasContext Context where
  context = id

hsPool :: Config -> IO (Pool HAS.Connection)
hsPool cfg = createPool createConn
  HAS.release
  (cfg ^. #numStripes)
  (cfg ^. #poolIdleTime)
  (cfg ^. #maxConnections)
 where
   createConn = do
     eConn <- HAS.acquire $ cfg ^. #connectionString
     case eConn of
       Left err -> fail $ show err
       Right conn -> pure conn

mkPool :: Config -> IO (Pool Connection)
mkPool cfg = createPool (connectPostgreSQL dbUrl)
  close
  (cfg ^. #numStripes)
  (cfg ^. #poolIdleTime)
  (cfg ^. #maxConnections)
 where
   dbUrl = cfg ^. #connectionString

runDBFunc :: DBPools -> DBConn a -> (a -> IO b) -> IO b
runDBFunc pool SimpleConn f = withResource (simplePool pool) f
runDBFunc pool HasqlConn f = withResource (hasqlPool pool) f

data DBPools = DBPools
  { simplePool :: Pool Simple.Connection
  , hasqlPool :: Pool HAS.Connection
  }

mkPoolMap :: Config -> IO DBPools
mkPoolMap cfg = do
  simplePool' <- mkPool cfg
  hasqlPool' <- hsPool cfg
  pure $ DBPools simplePool' hasqlPool'

newtype Context = Context { runPoolMap :: forall a b. DBConn a -> (a -> IO b) -> IO b }

mkContext :: Config -> IO Context
mkContext cfg = do
  mp <- mkPoolMap cfg
  pure $ Context $ runDBFunc mp

withPoolMap :: (HasContext env, MonadReader env m, MonadIO m) => DBConn a -> (a -> IO b) -> m b
withPoolMap connType f = do
  ctx <- view context
  liftIO $ runPoolMap ctx connType f
