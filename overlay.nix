final: prev:
{ haskell = prev.haskell //
            { packageOverrides = hpSelf: hpSuper:
                { postgres-context = hpSelf.callCabal2nix "postgres-context" ./. {};
                } // prev.haskell.packageOverrides hpSelf hpSuper;
            };
}
