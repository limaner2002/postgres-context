{
  description = "A library that provides a generic context for connecting to postgres using a pool";

  inputs =
    { nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;
      flake-utils.url = github:numtide/flake-utils;
    };

  outputs = { self, flake-utils, nixpkgs }:
    let overlay = import ./overlay.nix;
    in
      flake-utils.lib.eachDefaultSystem (system:
        rec { devShell = packages.haskellPackages.shellFor
              { packages = hp: [ hp.postgres-context
                               ];
                buildInputs = [ packages.haskellPackages.hlint
                                packages.haskellPackages.stylish-haskell
                              ];
              };
              defaultPackage = packages.haskellPackages.postgres-context;

              packages = import nixpkgs { inherit system;
                                          overlays = [ overlay ];
                                        };
            }
      ) // { inherit overlay; };
}

